package test

import (
	"gitee.com/MetaphysicCoding/task-flow"
	"slices"
	"testing"
	"time"
)

type Internal struct {
	Name int
}

type Src struct {
	Name     string
	Id       int
	Start    time.Time
	Internal Internal
	SameName string
	Exist    bool
}

type Dst struct {
	Name     string
	Id       int
	Start    time.Time
	Internal Internal
	SameName int
	UnExist  bool
}

func TestExplainStatus1(t *testing.T) {
	if !slices.Contains(task.ExplainStatus(task.Cancel), "Cancel") {
		t.Errorf("cancel status explain error")
	}
	if !slices.Contains(task.ExplainStatus(task.Pause), "Pause") {
		t.Errorf("pause status explain error")
	}
	if !slices.Contains(task.ExplainStatus(task.Running), "Running") {
		t.Errorf("running status explain error")
	}
	if !slices.Contains(task.ExplainStatus(task.Success), "Success") {
		t.Errorf("success status explain error")
	}
	if !slices.Contains(task.ExplainStatus(task.Failed), "Failed") {
		t.Errorf("failed status explain error")
	}
	if !slices.Contains(task.ExplainStatus(task.Timeout), "Timeout") {
		t.Errorf("timeout status explain error")
	}
	if !slices.Contains(task.ExplainStatus(task.Panic), "Panic") {
		t.Errorf("panic status explain error")
	}
	if !slices.Contains(task.ExplainStatus(task.Pending), "Pending") {
		t.Errorf("pending status explain error")
	}
}

func TestExplainStatus2(t *testing.T) {
	status := int64(0)
	task.AppendStatus(&status, task.Cancel)
	task.AppendStatus(&status, task.Panic)
	task.AppendStatus(&status, task.Failed)
	task.AppendStatus(&status, task.Timeout)
	task.AppendStatus(&status, task.Success)
	if slices.Contains(task.ExplainStatus(status), "Success") {
		t.Errorf("explain success while error occur")
	}
	if !slices.Contains(task.ExplainStatus(status), "Failed") {
		t.Errorf("failed status explain error")
	}
	if !slices.Contains(task.ExplainStatus(status), "Timeout") {
		t.Errorf("timeout status explain error")
	}
	if !slices.Contains(task.ExplainStatus(status), "Panic") {
		t.Errorf("panic status explain error")
	}
	if !slices.Contains(task.ExplainStatus(status), "Cancel") {
		t.Errorf("cancel status explain error")
	}
	status = 0
	task.AppendStatus(&status, task.Pause)
	task.AppendStatus(&status, task.Running)
	task.AppendStatus(&status, task.Pending)
	if !task.IsStatusNormal(status) {
		t.Errorf("normal status judge error")
	}
	if !slices.Contains(task.ExplainStatus(status), "Pause") {
		t.Errorf("pause status explain error")
	}
	if !slices.Contains(task.ExplainStatus(status), "Running") {
		t.Errorf("running status explain error")
	}
	if !slices.Contains(task.ExplainStatus(status), "Pending") {
		t.Errorf("pending status explain error")
	}
	task.AppendStatus(&status, task.Success)
	if !slices.Contains(task.ExplainStatus(status), "Success") {
		t.Errorf("success status explain error")
	}
}

func TestIsStatusNormal(t *testing.T) {
	normal := []int64{task.Pending, task.Running, task.Pause, task.Success}
	abnormal := []int64{task.Cancel, task.Timeout, task.Panic, task.Failed}
	for _, status := range normal {
		if !task.IsStatusNormal(status) {
			t.Errorf("normal status %d judge to abnormal", status)
		}
	}
	for _, status := range abnormal {
		if task.IsStatusNormal(status) {
			t.Errorf("abnormal status %d judge to normal", status)
		}
	}
}

func TestCreateStruct(t *testing.T) {
	src := Src{
		Internal: Internal{Name: 12},
		Name:     "src",
		Id:       12,
		Start:    time.Now(),
		SameName: "same",
		Exist:    true,
	}
	dst := task.CreateStruct[Dst](&src)
	if dst.Name != src.Name {
		t.Errorf("dst.Name != src.Name")
	}
	if dst.Id != src.Id {
		t.Errorf("dst.Id != src.Id")
	}
	if dst.Start != src.Start {
		t.Errorf("dst.Start != src.Start")
	}
	if dst.Internal != src.Internal {
		t.Errorf("dst.Internal != src.Internal")
	}
	if dst.SameName != 0 || dst.UnExist != false {
		t.Errorf("dst.SameName != 0 || dst.UnExist != false")
	}
	return
}

func TestCopyProperties(t *testing.T) {
	src := Src{
		Internal: Internal{Name: 12},
		Name:     "src",
		Id:       12,
		Start:    time.Now(),
		SameName: "same",
		Exist:    true,
	}
	dst := Dst{}
	task.CopyProperties(&src, &dst)
	if dst.Name != src.Name {
		t.Errorf("dst.Name != src.Name")
	}
	if dst.Id != src.Id {
		t.Errorf("dst.Id != src.Id")
	}
	if dst.Start != src.Start {
		t.Errorf("dst.Start != src.Start")
	}
	if dst.Internal != src.Internal {
		t.Errorf("dst.Internal != src.Internal")
	}
	if dst.SameName != 0 || dst.UnExist != false {
		t.Errorf("dst.SameName != 0 || dst.UnExist != false")
	}
	return
}
